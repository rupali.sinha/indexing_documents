import base64
import json
import os

from elasticsearch import Elasticsearch
import utils


es = Elasticsearch()


print(es.cat.health())
  
folder = './flask/static/data_split/'
body = {
  "description" : "Extract attachment information",
  "processors" : [
    {
      "attachment" : {
        "field" : "data"
      }
    }
  ]
}
es.index(index='_ingest', doc_type='pipeline', id='attachment', body=body)

list_data_file_names = os.listdir(folder)

for idx, file in enumerate(list_data_file_names):
    content = utils.parse_pdf(folder+file)
    message_bytes = content.encode('ascii', 'ignore')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')

    result2 = es.index(index='my_index', doc_type='my_type', id=idx+1, pipeline='attachment', body={'data': base64_message})
               
word_list = utils.create_word_list()

temp = {}
for word in word_list:
    res = es.search(index='my_index', doc_type='my_type', q=word, _source_exclude=['data'])
    doc_list = [doc['_id'] for doc in res['hits']['hits']]
    score_list = [doc['_score'] for doc in res['hits']['hits']]
    temp[word] = {'docs': doc_list, 'score': score_list}
    print(word)

    
with open('./search_results/inverted_index_splitv1.json', 'w', encoding="utf-8") as f:
  json.dump(temp, f, indent=4, ensure_ascii=False)
