$(document).ready(function() {

    $( "#submit" ).on( "click", function(event) {
        $.ajax({
			data : {
				searchWord : $('#searchWord').val()
			},
			type : 'POST',
			url : '/search'
		})
		.done(function(data) {

			if (data.error) {
				console.log('error');
			} 
			else {
                if (data.errorMsg || (data.file.length < 1)){
                    $('#resultArea').html('No documents retrieved!').show();
                    $('#pdfArea').hide();
                } else {
                    
                    var li = '<div class="col"><ul class="list-group list-group-horizontal">';
                    for (var i = 1; i <= data.file.length; i++) {
                        li += '<li class="list-group-item"><a href="#">\
                        '+data.file[i-1]+'</a></li>';
                    }
                    li += '</ul></div>';
                    
                    $('#resultArea').html(li).show();
                    $('#pdfArea').hide();

                }
                
            }

		});

		event.preventDefault();
    
    
    });

    $('#resultArea').on('click', 'li', function(){
        var fileName = $(this).text().trim();
        var pdf = "<iframe src='../static/data_split/"+fileName+"' width='100%' height='500px'></iframe>";
        $('#pdfArea').html(pdf).show();
    })

});