from elasticsearch import Elasticsearch
import base64
import os
import subprocess
import logging

logging.basicConfig(level=logging.INFO)

DATA = './flask/static/data_split/'

def parse_pdf(filename):
    """
    The function parses all the pdfs to be used as input
    for creating index structure.
    """
    try:
        content = subprocess.check_output(["pdftotext", '-enc', 'UTF-8', filename, "-"])
    except subprocess.CalledProcessError as e:
        logging.info('Skipping {} (pdftotext returned status {})'.format(filename, e.returncode))
        return None
    return content.decode('utf-8')


class ElasticSearch:
    """
    The class consists of the methods for creating and querying the index.
    """
    def __init__(self):
        self.es = Elasticsearch()
        logging.info('Health of the cluster: {}'.format(self.es.cat.health()))

        self.body = {
        "description" : "Extract attachment information",
        "processors" : [
            {
            "attachment" : {
                "field" : "data"
            }
            }
        ]
        }
        
    
    def schema(self):
        """Creates the schema of the index."""
        
        self.es.index(index='_ingest', doc_type='pipeline', id='attachment', body=self.body)
        
    def create_index(self):
        """Creates the index structure."""
        
        list_data_file_names = os.listdir(DATA)

        for idx, file in enumerate(list_data_file_names):
            content = parse_pdf(DATA+file)
            message_bytes = content.encode('ascii', 'ignore')
            base64_bytes = base64.b64encode(message_bytes)
            base64_message = base64_bytes.decode('ascii')

            result2 = self.es.index(index='my_index', doc_type='my_type', id=idx+1, pipeline='attachment', body={'data': base64_message})
            
        raw_data = self.es.indices.get_mapping('my_index')
        mapping_keys = raw_data['my_index']["mappings"].keys()
        doc_type = list(mapping_keys)[0]
        schema = raw_data['my_index']["mappings"][ doc_type ]["properties"]
        
            
    # def test_index(self):
    #     res = self.es.search(index='my_index', doc_type='my_type', q='Wintersemester', _source_exclude=['data'])

    #     return res
            
    def create_cluster(self):
        """Calls the utility methods."""
        
        self.schema()
        self.create_index()
        # self.test_index()
        
    def query_index(self, term):
        """Queries the index structure."""
        
        res = self.es.search(index='my_index', doc_type='my_type', q=term, _source_exclude=['data'])
        
        return res
 