# import necessary libraries
from flask import Flask, render_template, request
from flask.json import jsonify
import os
import logging
from create_es import ElasticSearch, DATA


logging.basicConfig(level=logging.INFO)

# initialize app
app = Flask(__name__)
app.config.from_pyfile('config.py')

# initialize elastic search cluster
es = ElasticSearch()


@app.route('/search', methods=['POST', 'GET'])
def search_from_es():
    """ 
    This function renders the html content when the user starts the app.
    """
    if request.method=='POST':
        
        search_word = request.form['searchWord']
        
        # call query to ES with the search word and return the pdfs
        res = es.query_index(search_word)
        id_list, temp = [], []
        list_data_file_names = os.listdir(DATA)

        if 'hits' in res:
            for doc in res['hits']['hits']:
                id_list.append(doc['_id'])
            
            for selected_idx in id_list:
                idx = int(selected_idx) - 1
                temp.append(list_data_file_names[idx])
           
            return jsonify({'file': temp})
        else:
            jsonify({'errorMsg': 'No document found'})
    
    return render_template('index.html')


@app.route('/')
def index():
    """ 
    This function renders the html content when the user starts the app.
    """
    es.create_cluster()
    
    return render_template('index.html')


if __name__ == '__main__':
    
    logging.info('Creating the elastic search cluster.')
    app.run(debug=True)