# Indexing Documents using Elastic Search

![Search](img/search.png)

This project is about retrieving relevant documents(individual pages) based on keyword search. An indexing structure is created using elastic search(ES) which is nothing but an inverted index. The project has two parts: <br>
A. code for creating analysis (writing the inverted index into a json file). <br>
B. code for a simple single page application where the user is asked to input any keyword and hence the relevant documents are displayed. <br>

# Project Structure
```
|____ flask/
|____ img/
|____ search_results/
|____ .gitignore
|____ create_results.py
|____ README.md
|____ utils.py
```


# Getting Started
There is no need to install ES locally. Simple execute below command to spin an ES container.

```
> docker run -d -p 9200:9200 elasticsearch-ingest 
```

Now, <br> 
A. for creating inverted index file for analysis purpose, execute:
```
> python create_results.py
```

B. for creating running the application:
```
> python app.py
```

Finally, <br>
to stop the container, execute:
```
> docker stop <container-name>
```

#### Important Notes
The `search_results` folder contains two files: `inverted_index_split.json` and `mapping.csv`. The first one is the inverted index structure of the relevant terms. The latter one is the mapping of index ids created by the elastic search engine to that of the data_split folder where all the data files are present. This mapping is one to one and in sequence.

# Application
Below is the screenshot of the single page application. This is only a simple demo and can be scaled up according to the requirement. The purpose of this application is only to demonstrate the implementaion of elastic search engine.

![App](img/app.png)