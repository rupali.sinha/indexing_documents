
from nltk.tokenize import word_tokenize
import string
from nltk.corpus import stopwords
import os
import subprocess
import pandas as pd


list_data_file_names = os.listdir("./data_split")

def parse_pdf(filename):
    """
    The function parses all the pdfs to be used as input
    for creating index structure.
    """
    try:
        content = subprocess.check_output(["pdftotext", '-enc', 'UTF-8', filename, "-"])
    except subprocess.CalledProcessError as e:
        print('Skipping {} (pdftotext returned status {})'.format(filename, e.returncode))
        return None
    return content.decode('utf-8')

def create_word_list():
    data_dict = {}

    for file in list_data_file_names:

        content = parse_pdf('./data_split/'+file)
        tokens = word_tokenize(content)
        # convert to lower case
        tokens = [w.lower() for w in tokens]
        # remove punctuation from each word

        table = str.maketrans('', '', string.punctuation)
        stripped = [w.translate(table) for w in tokens]
        # remove remaining tokens that are not alphabetic
        words = [word for word in stripped if word.isalpha()]

        # filter out stop words
        stop_words = set(stopwords.words('german'))
        words = [w for w in words if not w in stop_words]
        data_dict[file] = list(set(words))
        print('File {} is now finished.'.format(file))


    flat_list = [item for sublist in data_dict.values() for item in sublist]
    final_list = list(set(flat_list))

    final = [word for word in final_list if len(word) > 3]
    
    return final


def create_mapping_file():
    
    # dictionary of lists 
    dict = {'file': sorted(list_data_file_names), 'id': range(1, len(list_data_file_names) + 1)} 
        
    df = pd.DataFrame(dict)
    df.to_csv(r'./search_results/mapping.csv', index = False)
    
# create_mapping_file()